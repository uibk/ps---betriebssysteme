#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <wait.h>

#define PROC_NUMBER 12

int main(int argc, char const *argv[]) {
    printf("I am parent: %d\n", (int) getpid());
    pid_t pid;

    for (int i = 0; i < PROC_NUMBER; i++) {
        pid = fork();
        switch (pid) {
            case -1:
                perror("Fork failed.");
                exit(EXIT_FAILURE);
            case 0:
                printf("I am child: %d\n", (int) getpid());
                _exit(EXIT_SUCCESS);
            default:
                break;
        }
    }
    for (size_t i = 0; i < PROC_NUMBER; i++) {
        /*
         * < -1 for any child process whose process group ID is equal to pid
         * -1 for any child process
         * 0 for any child process whose process group ID is equal to the calling process (parent)
         * > 0 for exact pid value e.g. 5 would be process with pid 5
         */
        waitpid(-1, NULL, 0);
    }

    printf("%u child processes have been created.\n", PROC_NUMBER);
    return EXIT_SUCCESS;
}
/*
     Output is not consistent - child pid from previous creation often prints out later than later ones
     It is dependent on when child process gets resource (CPU)
*/
