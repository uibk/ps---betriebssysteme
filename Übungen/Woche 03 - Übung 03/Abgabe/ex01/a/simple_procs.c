#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <wait.h>

#define PROC_NUMBER 7

int main(int argc, char const *argv[]) {
    printf("I am parent: %d\n", (int) getpid());
    pid_t pid;

    for (size_t i = 0; i < PROC_NUMBER; i++) {
        pid = fork();
        switch (pid) {
            case -1:
                perror("Fork failed.");
                exit(EXIT_FAILURE);
            case 0:
                printf("I am child: %d\n", (int) getpid());
                _exit(EXIT_FAILURE);
            default:
                break;
        }
    }

    for (int i = 0; i < PROC_NUMBER; i++) {
        /*
            * < -1 for any child process whose process group ID is equal to pid
            * -1 for any child process
            * 0 for any child process whose process group ID is equal to the calling process (parent)
            * > 0 for exact pid value e.g. 5 would be process with pid 5
        */
        waitpid(-1, NULL, 0);
    }
    return EXIT_SUCCESS;
}
