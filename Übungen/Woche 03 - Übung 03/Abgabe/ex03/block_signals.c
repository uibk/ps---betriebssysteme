#define _XOPEN_SOURCE 500

#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <stdbool.h>

#define BLOCKING_TIME 13
#define WAITING_TIME 5

sigset_t block_mask;
pid_t pid;

void parent_handler(int signal) {
    switch (signal) {
        case SIGALRM:
            printf("[SIGALRM] received alarm...\n");
            // Send SIGUSR1 to child process
            kill(pid, SIGUSR1);
            break;
        case SIGCHLD:
            // Child has terminated and sends SIGCHLD to parent, also terminate parent process
            exit(EXIT_SUCCESS);
    }
}

void child_handler(int signal) {
    switch (signal) {
        case SIGUSR1:
            printf("[SIGUSR1] signal received...\n");
            break;
        case SIGUSR2:
            printf("[SIGUSR2] Child terminating...\n");
            _exit(EXIT_SUCCESS);
        case SIGALRM:
            // When child process gets SIGALRM signal, unblock signals in block_mask
            sigprocmask(SIG_UNBLOCK, &block_mask, NULL);
    }
}

int main(void) {
    printf("I am parent: %d\n", (int) getpid());
    pid_t child = fork();

    switch (child) {
        case -1:
            perror("fork()");
            exit(EXIT_FAILURE);
        case 0:
            printf("I am child: %d\n", (int) getpid());

            // Create own signal set for unblocking signals in child process
            sigset_t unblock_mask;
            sigemptyset(&unblock_mask);

            // Create sigaction struct for child to handle signals separately
            struct sigaction child_action;
            child_action.sa_mask = unblock_mask;
            child_action.sa_handler = child_handler;
            child_action.sa_flags = SA_NOCLDWAIT | SA_NOCLDSTOP;

            // First empty signal_set to properly add signals to the set
            sigemptyset(&block_mask);

            // Add SIGUSR2 to signal_set
            sigaddset(&block_mask, SIGUSR2);

            // Block SIGUSR2 which is in block_mask
            sigprocmask(SIG_BLOCK, &block_mask, NULL);

            // Set handlers before any alarm is being set
            sigaction(SIGUSR2, &child_action, NULL);
            sigaction(SIGUSR1, &child_action, NULL);
            sigaction(SIGALRM, &child_action, NULL);

            // Set alarm for 13 seconds defined in BLOCKING_TIME and block signals
            alarm(BLOCKING_TIME);
            /*
          Instead of a while loop maybe use sigwait() for wait for specific signal SIG_ALRM
          */
            while (1) {
                // Wait for all incoming signals
                pause();
            }

        default:;
            // Sleep for 5 seconds
            sleep(WAITING_TIME);

            // Send SIGUSR2 to child process
            pid = child;
            kill(pid, SIGUSR2);

            // Create new sigaction struct for parent handling their received signals
            struct sigaction parent_action;
            parent_action.sa_handler = parent_handler;
            parent_action.sa_flags = SA_NOCLDWAIT | SA_NOCLDSTOP;

            // Set actions for SIGALRM and SIGCHLD signal
            sigaction(SIGALRM, &parent_action, NULL);
            sigaction(SIGCHLD, &parent_action, NULL);

            while (1) {
                alarm(5);
                pause();
            }
    }
}
