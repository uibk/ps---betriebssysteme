#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <signal.h>
#include <wait.h>
#include <stdbool.h>

void signal_handler(int signal) {
    switch (signal) {
        case SIGCHLD:
            printf("[SIGCHLD] Child has terminated...\n");
            _exit(EXIT_SUCCESS);
        default:
            break;
    }
}

int main(void) {

    printf("I am parent: %d\n", (int) getpid());

    pid_t pid;
    pid = fork();
    switch (pid) {
        case -1:
            perror("Fork failed.");
            exit(EXIT_FAILURE);
        case 0:
            printf("I am child: %d\n", (int) getpid());
            _exit(EXIT_SUCCESS);
        default:;
            struct sigaction action;
            action.sa_handler = signal_handler;
            // Ensures that child processes doesn't turn into zombie processes
            // SA_NOCLDSTOP flag only sends SIGCHLD signal when child process is terminated and not stopped
            action.sa_flags = SA_NOCLDWAIT | SA_NOCLDSTOP;
            sigaction(SIGCHLD, &action, NULL);
            pause();
    }

    printf("Parent process is terminated...\n");
    return EXIT_SUCCESS;
}

/*
DEFINITION OF A ZOMBIE PROCESS
parent process doesn't provide functionality to wait for termination of child process and PID cannot be free'd up
*/
