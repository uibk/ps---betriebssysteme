#include <limits.h>
#include <stdio.h>
#include <math.h>

#include "scheduling_sim.h"
#include "scheduling_utility.h"

// ---------------------------------------------------------------------------
// Example schedulers
// ---------------------------------------------------------------------------

process_t *fcfs(const int __attribute__((unused)) timestep, processes_t *processes) {
    process_t *selected = NULL;
    int min_arrival_time = INT_MAX;

    for (int i = 0; i < processes->num_processes; ++i) {
        process_t *proc = processes->procs[i];

        if (proc->yielded) {
            proc->user1 = 0;
            continue;
        }

        if (proc->user1 == 1) {
            return proc;
        }

        if (proc->arrival_time < min_arrival_time) {
            min_arrival_time = proc->arrival_time;
            selected = proc;
        }
    }

    if (selected == NULL) {
        selected = processes->procs[0];
    }
    selected->user1 = 1;
    return selected;
}

process_t *round_robin(const int __attribute__((unused)) timestep, processes_t *processes_struct) {
    process_t **processes = processes_struct->procs;
    int num_processes = processes_struct->num_processes;

    process_t *selected = processes[0];
    int next_idx = 1 % num_processes;
    for (int i = 0; i < num_processes; ++i) {
        if (processes[i]->user1 == 1) {
            selected = processes[i];
            next_idx = (i + 1) % num_processes;
            break;
        }
    }

    selected->user1 = 0;
    process_t *next = processes[next_idx];
    if (next != selected) {
        next->user1 = 1;
    }
    return selected;
}

// ---------------------------------------------------------------------------
// Implement your schedulers here
// ---------------------------------------------------------------------------

process_t *round_robin_q3(const int __attribute__((unused)) timestep, processes_t *processes_struct) {
    process_t **processes = processes_struct->procs;
    int num_processes = processes_struct->num_processes;

    process_t *selected = processes[0];
    for (int i = 0; i < num_processes; ++i) {
        process_t *proc = processes[i];

        if (proc->yielded || proc->user1 == 3) {
            proc->user1 = 0;
            selected = processes[i + 1 % num_processes];
            break;
        } else if (proc->user1 > 0) {
            selected = proc;
            break;
        }
    }
    selected->user1 += 1;
    return selected;
}

process_t *srtf(const int __attribute__((unused)) timestep, processes_t *processes_struct) {
    process_t **processes = processes_struct->procs;
    int num_processes = processes_struct->num_processes;
    process_t *selected = processes[0];
    int min_remaining_time = INT_MAX;
    for (int i = 0; i < num_processes; ++i) {
        process_t *proc = processes[i];

        if (proc->yielded) {
            continue;
        } else if (proc->remaining_time < min_remaining_time) {
            min_remaining_time = proc->remaining_time;
            selected = proc;
        }
    }
    return selected;
}

process_t *priority_preemption(const int __attribute__((unused)) timestep, processes_t *processes_struct) {
    process_t **processes = processes_struct->procs;
    int num_processes = processes_struct->num_processes;
    process_t *selected = processes[0];
    int highest_priority = 0;
    for (int i = 0; i < num_processes; ++i) {
        process_t *proc = processes[i];

        if (proc->yielded) {
            continue;
        } else if (proc->priority > highest_priority) {
            highest_priority = proc->priority;
            selected = proc;
        }
    }
    return selected;
}

process_t *priority_non_preemption(const int __attribute__((unused)) timestep, processes_t *processes_struct) {
    process_t **processes = processes_struct->procs;
    int num_processes = processes_struct->num_processes;
    process_t *selected = processes[0];
    int highest_priority = 0;
    for (int i = 0; i < num_processes; ++i) {
        process_t *proc = processes[i];

        if (proc->yielded) {
            proc->user1 = 0;
        } else {
            if (proc->priority > highest_priority) {
                highest_priority = proc->priority;
                selected = proc;
            }
            if (proc->user1 != 0) {
                return proc;
            }
        }
    }
    selected->user1 = 1;

    return selected;
}

process_t *lottery(const int __attribute__((unused)) timestep, processes_t *processes_struct) {
    process_t *selected = NULL;
    const int QUANTUM = 3;
    const int TICKET_DISTR = 30;
    int num_processes = processes_struct->num_processes;

    //user1 stores the quantum of the current process
    //user2 stores the tickets of the processes
    //user3 stores if the tickets are valid, or if they have to be recalculated

    //when there is already a process running
    for (int i = 0; i < num_processes; i++) {
        process_t *currentproc = processes_struct->procs[i];

        float tickets = (float) TICKET_DISTR / currentproc->service_time;

        if (currentproc->user3 == 0) {
            if (currentproc->yielded) {
                currentproc->user2 = roundf(tickets * QUANTUM / (currentproc->user1));
                currentproc->user3 = 1;
            } else {
                currentproc->user2 = roundf(tickets);
                currentproc->user3 = 0;
            }
        }

        if (currentproc->user1) {
            if (currentproc->user1 < QUANTUM && !currentproc->yielded) {
                selected = currentproc;
            } else {
                currentproc->user1 = 0;
            }
        }
    }

    //if there is no active process which hasn't yielded, we need  to choose
    if (selected == NULL) {
        int ticket_sum = 0;

        for (int i = 0; i < num_processes; i++) {
            process_t *currentproc = processes_struct->procs[i];
            ticket_sum += currentproc->user2;
            printf("%c with %d\n", currentproc->name, currentproc->user2);
        }

        int result = scheduler_rand() % (ticket_sum) + 1;
        fprintf(stdout, "%d: %d --- total was %d\n", timestep, result, ticket_sum);
        int i = 0;
        int cumulated_propability = 0;
        do {
            selected = processes_struct->procs[i++];
            cumulated_propability += selected->user2;
        } while (result >= cumulated_propability);
    }

    selected->user1++;
    selected->user3 = 0;
    return selected;
}


// ---------------------------------------------------------------------------

int main(int argc, char **argv) {

    if (argc < 2) {
        fprintf(stderr, "Error: Usage: %s <filename>\nExample: %s input.csv\n", argv[0], argv[0]);
        return EXIT_FAILURE;
    }

    processes_t *procs = read_simulation_data(argv[1]);
    print_simulation_data(stdout, procs);

    // -----------------------------------------------------------------------
    // Insert calls to your schedulers here
    // -----------------------------------------------------------------------

    // print_schedule(stdout, compute_schedule(procs, fcfs), procs);
    // print_schedule(stdout, compute_schedule(procs, round_robin), procs);
    // print_schedule(stdout, compute_schedule(procs, srtf), procs);
//    print_schedule(stdout, compute_schedule(procs, round_robin_q3), procs);
//    print_schedule(stdout, compute_schedule(procs, priority_preemption), procs);
//    print_schedule(stdout, compute_schedule(procs, priority_non_preemption), procs);
    print_schedule(stdout, compute_schedule(procs, lottery), procs);
    // -----------------------------------------------------------------------

    free_processes(procs);

    return EXIT_SUCCESS;
}
