# Andreas Murk - Exercise Sheet 01 #

In this exercise you will be asked to work with a shell. The idea is to get familiar with some Unix commands and bash programming.

## Task 1 ##

Consider the following scripts. Explain what each line means within these scripts. Explain what happens when you execute these scripts with and without input arguments.

### Script 1 ###

```Bash
#!/bin/bash
for FN in "$@"
do
chmod 0750 "$FN"
done
```

### Explanation Script 1 ###

First line describes the shell in which this command will be executed. It is also called **Shebang #!**

When we execute this script it will using a for loop trough every parameter described in **$@** and change the permissions of the passed files to **0750**:

- **no permissions** for the whole **directory**
- **complete permissions** for the owner e.g. **root** or **\$USER**
- **read** and **execute permissions** for the **group**
- and **no permissions** for **others**

Have a look at the following picture:

![alt](img/dir.png)

We see two different *txt* files here which have same permissions, like **read** and **write** for **owner**, **read** for **group**, and **read** for **others**. When we now execute the shell script and pass these two files as parameters we get the following output:

![alt](img/dir2.png)

We can now cleary see that the permissions of the two passed files have been changed to **0750** like mentioned above.

### Script 2 ###

```Bash
#!/bin/bash
function usage {
    echo "$0: You must pass at least 3 parameters!"
    exit 1
}

ARG1=$1; shift || usage
ARG2=$1; shift || usage
ARG3=$1; shift || usage

grep -n "$ARG3" "$ARG1" > "$ARG2"
```

Hint: `shift` is a shell *built-in*, use `help shift` to get more information.

### Explanation Script 2 ###

We define a function *usage()* which provides an own defined error handling functionality. It is going to be invoked if the shift command haven't executed as intended with a *logical or ||*.

The *shift* command uses a shift operation to shift the value from parameters:

- The value **\$1** will be shifted to **\$2**
- **$2** shifted to **\$3** and so on.

Note that the value of **$1** will be lost if there isn't an assignment to a variable before the shift operation executes.

That means, we are assigning every **\$ARG** variable to **$1** but due to the fact that it is going to be shifted afterwards, the value of the next parameter **\$2** will be assigned to the next variable. In the end we basically have the same as the following assignment:

#### ARG1=$1 ####

#### ARG2=$2 ####

#### ARG3=$3 ####

Basically, *grep -n* outputs the line in which a pattern matches. So when I am looking for e.g. **"options"** in a file, instead of printing out the single matched word it will be the whole line. The following image shows this case:

![alt](img/grep.png)

The statement shown in the script tries to match the pattern given from **\$ARG3** in the content provided by **$ARG1** and the output of this search will be inserted into the file **\$ARG2**.

<br/>
<br/>

We can follow these steps to reproduce and show the output:

```Bash
touch test.txt # create empty file test.txt
touch test2.txt # create empty file test2.txt
echo "option" > test.txt # insert "option" to test.txt

# searches "option" ($ARG3) in test.txt ($ARG1)
# and writes output into test2.txt ($ARG2)
bash script2.sh test.txt test2.txt "option"

Output from test2.txt:
1:option
```

## Task 2 ##

What does this shell script do? Improve its usability by adding error handling for the following cases.

- Print a help message when the number of provided arguments is not two.
- Log an error message to a file *error.log* when the file *$OUTFILE* is not writable.

```Bash
#!/bin/bash
INFILE=$1
OUTFILE=$2
if [ -e "$INFILE" ]
then
    cat "$INFILE" >> "$OUTFILE"
fi
```

Hint: Take a look at the man page for the "test" command: *man "["*.

### Explanation Task 2 ###

We take two parameters *INFILE* and *OUTFILE* which represent an input and output file.

This script simply writes the content from the input-file (if the file exists) to the output-file using the command *cat*.

<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>

### Solution Script ###

```Bash
#!/bin/bash
INFILE=$1
OUTFILE=$2
ERROR="error.log"

if (($# < 2)) # if parameter counter is less than 2
then
    echo "You must provide 2 parameters!"
    exit 1
fi
if [ -w "$OUTFILE" ]; then # if $OUTFILE is writable
    echo "$OUTFILE is writable"
    if [ -e "$INFILE" ]; then # if $INFILE exists
        cat "$INFILE" >> "$OUTFILE"
fi
else
    echo "see $ERROR for error details"
    echo "$OUTFILE is not writable" > "$ERROR"
    exit 1
fi
```

We can follow these steps to reproduce and show the output:

```Bash
touch test.txt # create empty file test.txt
touch test2.txt # create empty file test2.txt
echo "HELLO" > test.txt # insert "HELLO" to test.txt

# writes content from $1 to $2 if $1 exists and $2 is writable
bash script3.sh test.txt test2.txt

Output from test2.txt:
HELLO
```

## Task 3 ##

Write a script **my_backup.sh** to perform a backup of the current directory (containing only files) into a given folder. The script receives a single parameter which is the path of the folder where the backup has to be stored. If the backup folder already exists then a file must be copied if and only if it does not exist within the backup folder or if it is newer than the existing one in the backup folder. The script will be used as follows

```Bash
# creates a backup of . into backup-folder
bash my_backup.sh /path/to/backup-folder
```

Hint: Use *stat --format %Y \<file>* to get the modification time of a file.

### Solution Backup Script ###

```Bash
#!/bin/bash
BACKUP_DIR="$1"
SRC_DIR="../Abgabe/"

# with rsync
# rsync -avP "$SRC_DIR" "$BACKUP_DIR"

if (($# < 1)) # if parameter counter is less than 1
then
    echo "You must specify a backup directory as parameter!"
    exit 1
fi

# copies all files from current directory to backup dir
if [ ! -d "$BACKUP_DIR" ]
then
    mkdir "$BACKUP_DIR"
fi
for FILENAME in ./*.*; do # looks for all data types in current directory
    # save current timestamp
    EXISTING_TIMESTAMP=$(stat --format %Y "$FILENAME")
    # only copy files when they are not existing
    if [ ! -e "$BACKUP_DIR/$FILENAME" ]
    then
        cp -v "$FILENAME" "$BACKUP_DIR"
    else
        # save timestamp of files in backup dir
        BACKUP_F_TIMESTAMP=$(stat --format %Y "$BACKUP_DIR/$FILENAME")
        if (( $EXISTING_TIMESTAMP > $BACKUP_F_TIMESTAMP )) 
        then
            cp -v "$FILENAME" "$BACKUP_DIR"   
        fi
    fi
done
```