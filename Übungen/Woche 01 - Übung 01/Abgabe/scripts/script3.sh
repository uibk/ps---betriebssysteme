#!/bin/bash
INFILE=$1
OUTFILE=$2
ERROR="error.log"

if (($# < 2)) # if parameter counter is less than 2
then
    echo "You must provide 2 parameters!"
    exit 1
fi
if [ -w "$OUTFILE" ]; then # if $OUTFILE is writable
    echo "$OUTFILE is writable"
    if [ -e "$INFILE" ]; then # if $INFILE exists
    	cat "$INFILE" >> "$OUTFILE"
fi
else
    echo "see $ERROR for error details"
    echo "$OUTFILE is not writable" > "$ERROR"
    exit 1
fi
