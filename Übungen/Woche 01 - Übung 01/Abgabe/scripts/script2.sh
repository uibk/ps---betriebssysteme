#!/bin/bash
function usage {
	echo "$0: You must pass at least 3 parameters!"
	exit 1
}

ARG1=$1; shift || usage
ARG2=$1; shift || usage
ARG3=$1; shift || usage


grep -n "$ARG3" "$ARG1" > "$ARG2"