#!/bin/bash
BACKUP_DIR="$1"
SRC_DIR="../Abgabe/"

# with rsync
# rsync -avP "$SRC_DIR" "$BACKUP_DIR"

if (($# < 1)) # if parameter counter is less than 1
then
    echo "You must specify a backup directory as parameter!"
    exit 1
fi
# copies all files from current directory to backup dir
if [ ! -d "$BACKUP_DIR" ]
then
    mkdir "$BACKUP_DIR"
fi
for FILENAME in ./*.*; do
    # save current timestamp
    EXISTING_TIMESTAMP=$(stat --format %Y "$FILENAME")
    # only copy files when they are not existing
    if [ ! -e "$BACKUP_DIR/$FILENAME" ]
    then
        cp -v "$FILENAME" "$BACKUP_DIR"
    else
        # save timestamp of files in backup dir
        BACKUP_F_TIMESTAMP=$(stat --format %Y "$BACKUP_DIR/$FILENAME")
        if (( $EXISTING_TIMESTAMP > $BACKUP_F_TIMESTAMP )) 
        then
            cp -v "$FILENAME" "$BACKUP_DIR"   
        fi
    fi
done