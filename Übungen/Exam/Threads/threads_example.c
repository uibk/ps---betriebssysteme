#include "threads_example.h"

pthread_mutex_t mutex;
void *thread_caller(void *arg) {
    fprintf(stdout, "I am Thread [%d]\n", *(int *) arg);
    pthread_exit(NULL);
}

int main(int argc, char const *argv[]) {
    pthread_t threads[THREAD_NUMBER];

    // Create specified threads and call thread_caller function
    for (long i = 0; i < THREAD_NUMBER; i++) {
        if ((pthread_create(&threads[i], NULL, thread_caller, (void *) &i)) != 0) {
            error_msg("pthread_create");
        }
    }

    // Wait for all threads to stop
    for (int i = 0; i < THREAD_NUMBER; i++) {
        if (pthread_join(threads[i], NULL) != 0) {
            error_msg("pthread_join");
        }
    }
    return EXIT_SUCCESS;
}
