// Compile it with attached CMakeLists.txt file in CLion or
// with gcc -std=gnu11 -Wall -Werror -pthread -lrt
#include "threads_example.h"
#include <string.h>
#include <stdbool.h>

#define BUF_SIZE 128
#define MESSAGES 15

unsigned int tickets = 1, current_ticket = 1;

char message[BUF_SIZE] = {0};
pthread_mutex_t mutex;
bool message_exist = false;

void *thread_caller(void *arg) {
    int read_messages = 0;
    unsigned int thread_ticket = 0;
    // If thread is logger thread
    if (strcmp((char *) arg, "logger") == 0) {
        while (read_messages < MESSAGES) {
            if (pthread_mutex_lock(&mutex) != 0) {
                error_msg_thread("pthread_mutex_lock");
            }
            // If no message exist, wait until message has been sent
            if (message_exist == false) {
                if (pthread_mutex_unlock(&mutex) != 0) {
                    error_msg_thread("pthread_mutex_lock");
                }
                continue;
            }
            fprintf(stdout, "%s", message);
            message[0] = 0;
            // Assumption: Message was deleted
            message_exist = false;
            // Increment current ticket
            current_ticket++;
            if (pthread_mutex_unlock(&mutex) != 0) {
                error_msg_thread("pthread_mutex_lock");
            }
            read_messages++;
        }
    } else {
        int i = 1;
        while (i <= 5) {
            if (pthread_mutex_lock(&mutex) != 0) {
                error_msg_thread("pthread_mutex_lock");
            }
            // If service thread has no ticket, give him one
            if (thread_ticket == 0) {
                thread_ticket = tickets++;
            }
            // If message already exists, continue and wait
            if (thread_ticket != current_ticket) {
                if (pthread_mutex_unlock(&mutex) != 0) {
                    error_msg_thread("pthread_mutex_lock");
                }
                continue;
            }
            sprintf(message, "%d %s\n", thread_ticket, (char *) arg);
            message_exist = true;
            if (pthread_mutex_unlock(&mutex) != 0) {
                error_msg_thread("pthread_mutex_lock");
            }
            // Reset ticket for service thread
            thread_ticket = 0;
            i++;
        }
    }
    pthread_exit(NULL);
}

int main(int argc, char const *argv[]) {
    pthread_t threads[THREAD_NUMBER];
    char *names[THREAD_NUMBER] = {"webserver", "weather-station", "database", "logger"};
    // Initialize pthread mutex
    if (pthread_mutex_init(&mutex, NULL) != 0) {
        error_msg("pthread_mutex_init");
    }

    // Create specified threads and call thread_caller function
    for (long i = 0; i < THREAD_NUMBER; i++) {
        if ((pthread_create(&threads[i], NULL, &thread_caller, (void *) names[i])) != 0) {
            error_msg("pthread_create");
        }
    }

    // Wait for all threads to stop
    for (int i = 0; i < THREAD_NUMBER; i++) {
        if (pthread_join(threads[i], NULL) != 0) {
            error_msg("pthread_join");
        }
    }

    // Release and destroy pthread mutex
    if (pthread_mutex_destroy(&mutex) != 0) {
        error_msg_thread("pthread_mutex_destroy");
    }
    return EXIT_SUCCESS;
}