// Compile it with attached CMakeLists.txt file in CLion or
// with gcc -std=gnu11 -Wall -Werror -pthread -lrt
#include "threads_example.h"
#include <string.h>
#include <stdbool.h>

#define BUF_SIZE 128
#define MESSAGES 15

unsigned int ticket = 0;

char message[BUF_SIZE] = {0};
pthread_mutex_t mutex;
pthread_cond_t message_cond, ticket_cond;
bool message_exist = false;

void *thread_caller(void *arg) {
    // If thread is logger thread
    if (strcmp((char *) arg, "logger") == 0) {
        int i = 1;
        while (i <= MESSAGES + 1) {
            if (pthread_mutex_lock(&mutex) != 0) {
                error_msg_thread("pthread_mutex_lock");
            }
            // If ticket is not zero, wait (only write when ticket is zero)
            while (ticket != 0) {
                pthread_cond_wait(&message_cond, &mutex);
            }
            fprintf(stdout, "%s", message);
            // Increment current ticket
            ticket = i++;
            // When current ticket has incremented send signal to service threads
            pthread_cond_signal(&ticket_cond);
            // Unlock mutex
            if (pthread_mutex_unlock(&mutex) != 0) {
                error_msg_thread("pthread_mutex_lock");
            }
        }
    } else {
        int i = 1;
        while (i <= 5) {
            if (pthread_mutex_lock(&mutex) != 0) {
                error_msg_thread("pthread_mutex_lock");
            }
            // If ticket from thread is not equal to logger ticket, wait until is equal
            while (ticket == 0) {
                pthread_cond_wait(&ticket_cond, &mutex);
            }
            sprintf(message, "%d %s\n", ticket, (char *) arg);
            ticket = 0;
            // If message is in message buffer, send signal to logger thread
            pthread_cond_signal(&message_cond);
            if (pthread_mutex_unlock(&mutex) != 0) {
                error_msg_thread("pthread_mutex_lock");
            }
            // Reset ticket for service thread
            i++;
        }
    }
    pthread_exit(NULL);
}

int main(int argc, char const *argv[]) {
    pthread_t threads[THREAD_NUMBER];
    char *names[THREAD_NUMBER] = {"webserver", "weather-station", "database", "logger"};
    // Initialize pthread mutex
    if (pthread_mutex_init(&mutex, NULL) != 0) {
        error_msg("pthread_mutex_init");
    }

    if (pthread_cond_init(&message_cond, NULL) || pthread_cond_init(&ticket_cond, NULL) != 0) {
        error_msg("pthread_cond_init");
    }

    // Create specified threads and call thread_caller function
    for (long i = 0; i < THREAD_NUMBER; i++) {
        if ((pthread_create(&threads[i], NULL, &thread_caller, (void *) names[i % THREAD_NUMBER])) != 0) {
            error_msg("pthread_create");
        }
    }

    // Wait for all threads to stop
    for (int i = 0; i < THREAD_NUMBER; i++) {
        if (pthread_join(threads[i], NULL) != 0) {
            error_msg("pthread_join");
        }
    }
    // Release and destroy pthread mutex
    if (pthread_cond_destroy(&message_cond) || pthread_cond_destroy(&ticket_cond) != 0) {
        error_msg_thread("pthread_mutex_destroy");
    }
    // Release and destroy pthread mutex
    if (pthread_mutex_destroy(&mutex) != 0) {
        error_msg_thread("pthread_mutex_destroy");
    }
    return EXIT_SUCCESS;
}