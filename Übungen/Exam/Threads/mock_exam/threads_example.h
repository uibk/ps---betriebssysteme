#define _GNU_SOURCE
#define _POSIX_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/syscall.h>
#include <ctype.h>
#include <pthread.h>

#define THREAD_NUMBER 4

#ifndef PS_BETRIEBSSYSTEME_THREADS_EXAMPLE_H
#define PS_BETRIEBSSYSTEME_THREADS_EXAMPLE_H

void *thread_caller(void *arg);

void error_msg(char *msg) {
    perror(msg);
    exit(EXIT_FAILURE);
}

void error_msg_thread(char *msg) {
    perror(msg);
    pthread_exit(NULL);
}

#endif //PS_BETRIEBSSYSTEME_THREADS_EXAMPLE_H

