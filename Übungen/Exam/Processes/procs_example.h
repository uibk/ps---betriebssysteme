#ifndef PS_BETRIEBSSYSTEME_PROCS_EXAMPLE_H
#define PS_BETRIEBSSYSTEME_PROCS_EXAMPLE_H

#define _GNU_SOURCE
#define _POSIX_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/syscall.h>
#include <ctype.h>
#include <wait.h>

#define PROC_NUMBER 1
#define ITERATIONS 1

void parent_error_msg(char *msg) {
    perror(msg);
    exit(EXIT_FAILURE);
}

void child_error_msg(char *msg) {
    perror(msg);
    _exit(EXIT_FAILURE);
}

void parent_signal_handler(int signal) {
    switch (signal) {
        case SIGALRM:
            //TODO: implement action for SIGARLM for parent process
            break;
        case SIGCHLD:
            //TODO: implement action for SIGCHLD for parent process
            exit(EXIT_SUCCESS);
        default:
            break;
    }
}

void child_signalhandler(int signal) {
    switch (signal) {
        case SIGUSR1:
            //TODO: implement action for SIGUSR1 for child process
            _exit(EXIT_SUCCESS);
        case SIGUSR2:
            //TODO: implement action for SIGUSR2 for child process
            _exit(EXIT_SUCCESS);
        case SIGALRM:
            //TODO: implement action for SIGARLM for child process
            _exit(EXIT_SUCCESS);
        default:
            break;
    }
}

#endif //PS_BETRIEBSSYSTEME_PROCS_EXAMPLE_H
