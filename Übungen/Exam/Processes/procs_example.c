#include "procs_example.h"

int main(int argc, char const *argv[]) {
    printf("I am parent: %d\n", (int) getpid());
    pid_t pid;

    for (size_t i = 0; i < PROC_NUMBER; i++) {
        pid = fork();
        switch (pid) {
            case -1:
                parent_error_msg("fork");
            case 0:
                printf("I am child: %d\n", (int) getpid());
                //TODO: implement child behaviour here
                _exit(EXIT_SUCCESS);
            default:
                break;
        }
    }

    // Wait for all child to terminate -> either trough signals or waitpid
    for (int i = 0; i < PROC_NUMBER; i++) {
        /*
            * < -1 for any child process whose process group ID is equal to pid
            * -1 for any child process
            * 0 for any child process whose process group ID is equal to the calling process (parent)
            * > 0 for exact pid value e.g. 5 would be process with pid 5
        */
        waitpid(-1, NULL, 0);
    }
    return EXIT_SUCCESS;
}
