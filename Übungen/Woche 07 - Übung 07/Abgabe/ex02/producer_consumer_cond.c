#define _POSIX_SOURCE 
#define _GNU_SOURCE 
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdbool.h>
#include <sys/types.h>
#include <pthread.h>

#include "myqueue.h"

#define THREAD_NUMBER 5
#define ITERATIONS 10000

pthread_mutex_t pthread_mutex;
pthread_cond_t pthread_cond;

void printQueue()
{
    printf("length: %u", size());
    if (!empty())
    {
        printf(", front: %u", front());
    }
    printf("\n");
}

void* consume()
{
    unsigned int sum = 0, result = 0;
    long tid = pthread_self();
    while (true)
    {
        pthread_mutex_lock(&pthread_mutex);
        
        while(empty()) {
            pthread_cond_wait(&pthread_cond, &pthread_mutex);
        }

        result = front();
        pop();
        if (result == 1)
        {
            sum += result;
        }
        else
        {
            pthread_mutex_unlock(&pthread_mutex);
            break;
        }
        pthread_mutex_unlock(&pthread_mutex);
    }
    printf("Thread-ID %ld: %u\n", tid, sum);
    pthread_exit(NULL);
}

void produce()
{
    // To ensure that while producing consumer can already consume
    for (int i = 0; i < ITERATIONS; i++)
    {
        pthread_mutex_lock(&pthread_mutex);
        push(1);
        // Send signal when an element is available to all threads
        pthread_cond_signal(&pthread_cond);
        pthread_mutex_unlock(&pthread_mutex);
    }

    for (int j = 0; j < 5; j++)
    {
        pthread_mutex_lock(&pthread_mutex);
        push(0);
        // Send signal when an element is available to all threads
        pthread_cond_signal(&pthread_cond);
        pthread_mutex_unlock(&pthread_mutex);
    }
}

int main(int argc, char const *argv[])
{
    create();
    pthread_t threads[THREAD_NUMBER];

    pthread_mutex_init(&pthread_mutex, NULL);

    pthread_cond_init(&pthread_cond, NULL);

    for (long i = 0; i < THREAD_NUMBER; i++)
    {
        if ((pthread_create(&threads[i], NULL, consume, (void *) i)) != 0) {
            perror("pthread_create");
            _exit(EXIT_FAILURE);
        }
    }

    produce();

    // Wait for all threads to stop
    for (int i = 0; i < THREAD_NUMBER; i++)
    {
        if (pthread_join(threads[i], NULL) != 0)
        {
            perror("pthread_join");
        }
        
    }

    pthread_cond_destroy(&pthread_cond);    

    pthread_mutex_destroy(&pthread_mutex);

    return EXIT_SUCCESS;
}

