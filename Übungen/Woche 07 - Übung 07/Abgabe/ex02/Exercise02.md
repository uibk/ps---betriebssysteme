###### Condition variables compared to spinlocks and mutex

- The condition variables simply allows a thread to signal all other waiting threads when something happens (in this case, when a new element is available)
- The POSIX mutex doesn't allow and therefore doesn't do it
- an approach which doesn't include condition variables would be to whenever a consumer wants to get something from the queue, it needs to poll the queue, take the element and release the mutex on each poll
- The mutex only allows you to wait until the lock is available or released
- a condition variable is generally used to avoid busy waiting