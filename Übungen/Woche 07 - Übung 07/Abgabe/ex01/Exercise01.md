##### Time measurement (1000 Threads 10000 Iterations)

###### Mutex implementation

<img src="time_measure_week06_ex02.png">

###### Spinlock implementation

<img src="time_measure_week07_ex01.png">

##### Time measurement (10 Threads 10000 Iterations)

###### Mutex implementation

<img src="time_measure_week06_ex02_2.png">

###### Spinlock implementation

<img src="time_measure_week07_ex01_2.png">

###### Interpretation

- spinlocks implementation often got higher CPU  percentage (probably busy waiting)
- spinlock uses nearly zero seconds on system time
- mutex implementation have higher voluntary context switches and spinlock involuntary context switches