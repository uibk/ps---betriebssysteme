#define _POSIX_SOURCE
#define _GNU_SOURCE

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdbool.h>
#include <sys/types.h>
#include <sys/time.h>
#include <pthread.h>

#include "myqueue.h"

#define THREAD_NUMBER 10
#define ITERATIONS 10000

pthread_spinlock_t spin_lock;

void printQueue() {
    printf("length: %u", size());
    if (!empty()) {
        printf(", front: %u", front());
    }
    printf("\n");
}

void *consume() {
    unsigned int sum = 0, result = 0;
    long tid = pthread_self();
    while (true) {
        pthread_spin_lock(&spin_lock);
        if (!empty()) {
            result = front();
            pop();
            if (result == 1) {
                sum += result;
            } else {
                pthread_spin_unlock(&spin_lock);
                break;
            }
        }
        pthread_spin_unlock(&spin_lock);
    }
    printf("Thread-ID %ld: %u\n", tid, sum);
    pthread_exit(NULL);
}

void produce() {
    // To ensure that while producing consumer can already consume
    pthread_spin_lock(&spin_lock);
    for (int i = 0; i < ITERATIONS; i++) {
        push(1);
    }
    pthread_spin_unlock(&spin_lock);

    pthread_spin_lock(&spin_lock);

    for (int j = 0; j < THREAD_NUMBER; j++) {
        push(0);
    }
    pthread_spin_unlock(&spin_lock);
}

int main(int argc, char const *argv[]) {
    create();
    pthread_t threads[THREAD_NUMBER];
    int consumer;
    // If a nonzero value is given, the spinlock can be shared between different threads/processes
    pthread_spin_init(&spin_lock, 0);

    for (long i = 0; i < THREAD_NUMBER; i++) {
        consumer = pthread_create(&threads[i], NULL, consume, (void *) i);

        if (consumer) {
            perror("pthread_create");
            _exit(EXIT_FAILURE);
        }
    }

    produce();

    // Wait for all threads to stop
    for (int i = 0; i < THREAD_NUMBER; i++) {
        if (pthread_join(threads[i], NULL) != 0) {
            perror("pthread_join");
            _exit(EXIT_FAILURE);
        }
    }

    pthread_spin_destroy(&spin_lock);

    return EXIT_SUCCESS;
}
// spinlocks are faster due to the fact that they put tasks into non-sleep status (busy-waiting) but therefore consumes more CPU loadout