##### Time measurement (1000 Threads and 10000 Iterations)

###### without atomic

<img src="time_measure_week07_ex03_a.png">

###### with atomic operation

<img src="time_measure_week07_ex03_b.png">

##### Time measurement (1000 Threads and 1000000 Iterations)

###### without atomic

<img src="time_measure_week07_ex03_a_2.png">

###### with atomic operation

<img src="time_measure_week07_ex03_b_2.png">

###### Interpretation

- normal approach without any atomics is way faster in execution than with atomics
- without atomics different threads get access and overwrites value and strange results are printed out (e. g. 19225841 etc.)
- we can assure that the right result is printed out when using c11 atomics as the variable x is declared as an atomic variable