#include <stdatomic.h>
#include "threads_example.h"

atomic_int x = 0;
// int x = 0;

void *thread_caller(void *arg) {
    for (int i = 0; i < ITERATIONS; i++) {
        // in Terminal -> type in 'module avail' for all available modules
        // then with 'module load gcc/7.4.0' you can load required modules for stdatomic.h library

        /* memory order relaxed ensures that the atomical operation keeps atomic 
        but the order can be changed from the compiler 
        (e. g. other memory-position for given variable could gives performance boost)
        */

        // without stdatomic.h header file
        // __atomic_fetch_add(&x, 1, __ATOMIC_SEQ_CST); 
        // __atomic_fetch_add_explicit(&x, 1, memory_order_relaxed); 
        x++;
    }
    pthread_exit(NULL);
}

int main(int argc, char const *argv[]) {
    pthread_t threads[THREAD_NUMBER];
    int thread;


    for (long i = 0; i < THREAD_NUMBER; i++) {
        thread = pthread_create(&threads[i], NULL, thread_caller, (void *) i);

        if (thread) {
            perror("pthread_create");
            _exit(EXIT_FAILURE);
        }
    }

    // Wait for all threads to stop
    for (int i = 0; i < THREAD_NUMBER; i++) {
        if (pthread_join(threads[i], NULL) != 0) {
            perror("pthread_join");
            exit(EXIT_FAILURE);
        }
    }

    printf("Value of x = %d\n", x);

    return EXIT_SUCCESS;
}
