#include "threads_example.h"

int x = 0;

void *thread_caller(void *arg)
{
    for (int i = 0; i < ITERATIONS; i++)
    {
        x++;
    }

    pthread_exit(NULL);
}

int main(int argc, char const *argv[])
{
    pthread_t threads[THREAD_NUMBER];
    int thread;

    for (long i = 0; i < THREAD_NUMBER; i++)
    {
        thread = pthread_create(&threads[i], NULL, thread_caller, (void *)i);

        if (thread)
        {
            perror("pthread_create");
            _exit(EXIT_FAILURE);
        }
    }

    // Wait for all threads to stop
    for (int i = 0; i < THREAD_NUMBER; i++)
    {
        if (pthread_join(threads[i], NULL) != 0)
        {
            perror("pthread_join");
            exit(EXIT_FAILURE);
        }
    }

    printf("Value of x = %d\n", x);

    return EXIT_SUCCESS;
}
