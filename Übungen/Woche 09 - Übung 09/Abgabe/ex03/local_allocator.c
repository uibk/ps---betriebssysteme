#define _POSIX_SOURCE
#define _GNU_SOURCE
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include "allocator.h"

typedef struct memory
{
    long size;
    long allocs;
} memory_t;

memory_t *memory;
pthread_mutex_t mutex;
void *thread_caller(void *arg)
{
    init_allocator();
    int *pointer_arr[memory->allocs];
    for (int i = 0; i < memory->allocs; i++)
    {
        pthread_mutex_lock(&mutex);
        pointer_arr[i] = my_malloc(memory->size);
        pthread_mutex_unlock(&mutex);
        fprintf(stdout, "Thread [%d] -> alloc %d done\n", *(int *)arg, i);
        if (pointer_arr[i] == NULL)
        {
            perror("malloc");
            exit(EXIT_FAILURE);
        }
    }

    for (int i = 0; i < memory->allocs; i++)
    {
        my_free(pointer_arr[i]);
    }
    destroy_allocator();
    pthread_exit(NULL);
}

int main(int argc, char const *argv[])
{
    pthread_mutex_init(&mutex, NULL);
    if (argc < 4)
    {
        printf("You must specify at least 4 arguments!");
        exit(EXIT_FAILURE);
    }
    else
    {
        long THREAD_NUMBER;
        int rc;
        memory = malloc(sizeof(*memory));
        THREAD_NUMBER = strtol(argv[1], NULL, 10);
        memory->allocs = strtol(argv[2], NULL, 10);
        memory->size = strtol(argv[3], NULL, 10);

        pthread_t threads[THREAD_NUMBER];
        for (int i = 0; i < THREAD_NUMBER; i++)
        {
            if ((pthread_create(&threads[i], NULL, &thread_caller, (void *) &i)) < 0)
            {
                perror("pthread_create");
                exit(EXIT_FAILURE);
            }
        }

        for (int i = 0; i < THREAD_NUMBER; i++)
        {
            if (pthread_join(threads[i], NULL) < 0)
            {
                perror("pthread_join");
                exit(EXIT_FAILURE);
            }
        }
    }
    pthread_mutex_destroy(&mutex);
    free(memory);
    memory = NULL;
    return EXIT_SUCCESS;
}

/*
With 100 Threads, 1000 Allocations of each 1024 kBit
Command being timed: "./local_allocator 100 1000 1024"
        User time (seconds): 0.33
        System time (seconds): 0.46
        Percent of CPU this job got: 113%
        Elapsed (wall clock) time (h:mm:ss or m:ss): 0:00.70
        Average shared text size (kbytes): 0
        Average unshared data size (kbytes): 0
        Average stack size (kbytes): 0
        Average total size (kbytes): 0
        Maximum resident set size (kbytes): 73580
        Average resident set size (kbytes): 0
        Major (requiring I/O) page faults: 0
        Minor (reclaiming a frame) page faults: 25953
        Voluntary context switches: 88099
        Involuntary context switches: 187
        Swaps: 0
        File system inputs: 0
        File system outputs: 0
        Socket messages sent: 0
        Socket messages received: 0
        Signals delivered: 0
        Page size (bytes): 4096
        Exit status: 0
*/

/*
With 100 Threads, 3000 Allocations of each 1024 kBit

    Command being timed: "./local_allocator 100 3000 1024"
        User time (seconds): 4.18
        System time (seconds): 0.98
        Percent of CPU this job got: 206%
        Elapsed (wall clock) time (h:mm:ss or m:ss): 0:02.51
        Average shared text size (kbytes): 0
        Average unshared data size (kbytes): 0
        Average stack size (kbytes): 0
        Average total size (kbytes): 0
        Maximum resident set size (kbytes): 209196
        Average resident set size (kbytes): 0
        Major (requiring I/O) page faults: 0
        Minor (reclaiming a frame) page faults: 77151
        Voluntary context switches: 199832
        Involuntary context switches: 13952
        Swaps: 0
        File system inputs: 0
        File system outputs: 0
        Socket messages sent: 0
        Socket messages received: 0
        Signals delivered: 0
        Page size (bytes): 4096
        Exit status: 0
*/