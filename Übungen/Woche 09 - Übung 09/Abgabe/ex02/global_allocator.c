#define _POSIX_SOURCE
#define _GNU_SOURCE

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include "allocator.h"

typedef struct memory {
    long size;
    long allocs;
} memory_t;

memory_t *memory;
pthread_mutex_t mutex;

void *thread_caller(void *arg) {
    int *pointer_arr[memory->allocs];
    for (int i = 0; i < memory->allocs; i++) {
        if (pthread_mutex_lock(&mutex) != 0) {
            error_msg("pthread_mutex_lock");
        }
        pointer_arr[i] = my_malloc(memory->size);
        if (pthread_mutex_unlock(&mutex) != 0) {
            error_msg("pthread_mutex_unlock");
        }
        fprintf(stdout, "Thread [%d] -> alloc %d done\n", *(int *) arg, i);
        if (pointer_arr[i] == NULL) {
            perror("malloc");
            exit(EXIT_FAILURE);
        }
    }

    for (int i = 0; i < memory->allocs; i++) {
        my_free(pointer_arr[i]);
    }

    pthread_exit(NULL);
}

int main(int argc, char const *argv[]) {
    pthread_mutex_init(&mutex, NULL);
    init_allocator();
    if (argc < 4) {
        printf("You must specify at least 4 arguments!");
        exit(EXIT_FAILURE);
    } else {
        long THREAD_NUMBER;
        memory = malloc(sizeof(*memory));
        THREAD_NUMBER = strtol(argv[1], NULL, 10);
        memory->allocs = strtol(argv[2], NULL, 10);
        memory->size = strtol(argv[3], NULL, 10);

        pthread_t threads[THREAD_NUMBER];
        for (int i = 0; i < THREAD_NUMBER; i++) {
            if (pthread_create(&threads[i], NULL, &thread_caller, (void *) &i) < 0) {
                error_msg("pthread_create");
            }
        }

        for (int i = 0; i < THREAD_NUMBER; i++) {
            if (pthread_join(threads[i], NULL) < 0) {
                error_msg("pthread_join");
            }
        }
    }
    if (pthread_mutex_destroy(&mutex) != 0) {
        error_msg("pthread_mutex_destroy");
    }
    destroy_allocator();
    free(memory);
    memory = NULL;
    return EXIT_SUCCESS;
}

/*
With 100 Threads, 1000 Allocations of each 1024 kBit

Command exited with non-zero status 1
        Command being timed: "./global_allocator 100 1000 1024"
        User time (seconds): 0.03
        System time (seconds): 0.19
        Percent of CPU this job got: 72%
        Elapsed (wall clock) time (h:mm:ss or m:ss): 0:00.31
        Average shared text size (kbytes): 0
        Average unshared data size (kbytes): 0
        Average stack size (kbytes): 0
        Average total size (kbytes): 0
        Maximum resident set size (kbytes): 47960
        Average resident set size (kbytes): 0
        Major (requiring I/O) page faults: 0
        Minor (reclaiming a frame) page faults: 11602
        Voluntary context switches: 38468
        Involuntary context switches: 158
        Swaps: 0
        File system inputs: 0
        File system outputs: 0
        Socket messages sent: 0
        Socket messages received: 0
        Signals delivered: 0
        Page size (bytes): 4096
        Exit status: 1
*/

/*
With 100 Threads, 3000 Allocations of each 1024 kBit

Command exited with non-zero status 1
        Command being timed: "./global_allocator 100 3000 1024"
        User time (seconds): 0.62
        System time (seconds): 0.87
        Percent of CPU this job got: 83%
        Elapsed (wall clock) time (h:mm:ss or m:ss): 0:01.78
        Average shared text size (kbytes): 0
        Average unshared data size (kbytes): 0
        Average stack size (kbytes): 0
        Average total size (kbytes): 0
        Maximum resident set size (kbytes): 217796
        Average resident set size (kbytes): 0
        Major (requiring I/O) page faults: 0
        Minor (reclaiming a frame) page faults: 54076
        Voluntary context switches: 193257
        Involuntary context switches: 334
        Swaps: 0
        File system inputs: 0
        File system outputs: 0
        Socket messages sent: 0
        Socket messages received: 0
        Signals delivered: 0
        Page size (bytes): 4096
        Exit status: 1
*/