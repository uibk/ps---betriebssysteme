#include <stdlib.h>
#include <stdio.h>
#include <limits.h>

#define TOTAL_SIZE 1073741824 // => 1 Gigabyte

void error_msg(char *msg) {
    perror(msg);
    exit(EXIT_FAILURE);
}

typedef struct block
{
    size_t size;
    struct block *next;
} Block;

Block *head;

void init_allocator()
{
    head = (Block *)malloc(TOTAL_SIZE);
    head->size = 0;
    head->next = head + sizeof(Block);

    Block *next_block = head->next;
    next_block->size = TOTAL_SIZE - sizeof(Block) * 2;
    next_block->next = NULL;
}

void destroy_allocator()
{
    free(head);
}
void *my_malloc(size_t size)
{

    Block *current = head->next;
    Block *before_current = head;
    Block *best = current;
    Block *before_best = head;

    while (current != NULL)
    {
        // If size of block is higher than to be allocated size and smaller than best block size
        if (current->size >= size && current->size < best->size)
        {
            best = current;
            before_best = before_current;
        }
        before_current = current;
        current = current->next;
    }

    if (best == NULL)
    {
        printf("Cannot find a block which suits to %ld bytes", size);
        exit(EXIT_FAILURE);
    }

    if (best->size > (size + sizeof(Block)))
    {
        // new block is allocated at the end of previous best sized block
        Block *new_block = size + sizeof(Block) + (void *)best;
        new_block->size = best->size - sizeof(Block) - size;
        new_block->next = best->next;
        // new best block size is shrunked
        best->size = size;
        // pointer not need anymore
        best->next = NULL;
        before_best->next = new_block;
    }
    else
    {
        // e. g. exact match of block , simply pass current block 
        before_best->next = best->next;
        // remove links to its next
        best->next = NULL;
    }
    // get address of best block
    void *address = best + sizeof(Block);
    return (void *)address;
}

void merge_blocks(Block *first_block, Block *second_block)
{
    // If end of first block containing alignment and data is equal to address space of second block
    if ((first_block + first_block->size + sizeof(Block) == second_block))
    {
        // Simply cover memory areas of first and second block
        first_block->next = second_block->next;
        first_block->size += second_block->size + sizeof(Block);
    }
}

void my_free(void *address)
{
    // to be free'd adress is given adress minus alignment of struct
    Block *freed_block = address - sizeof(Block);

    Block *current = head;

    // find block which is smaller than to be free'd block to insert after
    while (current != NULL && current->next != NULL && current->next < freed_block)
    {
        current = current->next;
    }

    if (current == NULL)
    {
        printf("Unable to find responsible block for given adress %p\n", address);
        exit(EXIT_FAILURE);
    }
    // Re-add block to pipe of free blocks
    freed_block->next = current->next;
    current->next = freed_block;

    // Merge blocks together
    merge_blocks(current, freed_block);
    merge_blocks(freed_block, freed_block->next);
}