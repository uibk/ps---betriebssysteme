#define _POSIX_SOURCE
#define _GNU_SOURCE

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <pthread.h>

typedef struct memory {
    long size;
    long allocs;
} memory_t;

memory_t *memory;
int **pointer_arr;

void *thread_caller(void *arg) {
    int *pointer_arr[memory->allocs];
    for (int i = 0; i < memory->allocs; i++) {
        pointer_arr[i] = malloc(memory->size);
        fprintf(stdout, "Thread [%d] -> alloc %d done\n", *(int *) arg, i);
        if (pointer_arr[i] == 0) {
            perror("malloc");
            exit(EXIT_FAILURE);
        }
    }

    for (int i = 0; i < memory->allocs; i++) {
        free(pointer_arr[i]);
    }

    pthread_exit(NULL);
}

int main(int argc, char const *argv[]) {
    if (argc < 4) {
        printf("You must specify at least 4 arguments!");
        exit(EXIT_FAILURE);
    } else {
        long THREAD_NUMBER;
        int rc;
        memory = malloc(sizeof(*memory));
        THREAD_NUMBER = strtol(argv[1], NULL, 10);
        memory->allocs = strtol(argv[2], NULL, 10);
        memory->size = strtol(argv[3], NULL, 10);
        pthread_t threads[THREAD_NUMBER];
        for (int i = 0; i < THREAD_NUMBER; i++) {
            if ((rc = pthread_create(&threads[i], NULL, &thread_caller, (void *) &i)) < 0) {
                perror("pthread_create");
                exit(EXIT_FAILURE);
            }
        }

        for (int i = 0; i < THREAD_NUMBER; i++) {
            if (pthread_join(threads[i], NULL) < 0) {
                perror("pthread_join");
                exit(EXIT_FAILURE);
            }
        }
    }
    free(memory);
    memory = NULL;
    return EXIT_SUCCESS;
}

/*\
With 100 Threads, 1000 Allocations of each 1024 kBit\
Command exited with non-zero status 1\
        Command being timed: "./global_allocator 100 1000 1024"\
        User time (seconds): 0.03\
        System time (seconds): 0.19\
        Percent of CPU this job got: 72%\
        Elapsed (wall clock) time (h:mm:ss or m:ss): 0:00.31\
        Average shared text size (kbytes): 0\
        Average unshared data size (kbytes): 0\
        Average stack size (kbytes): 0\
        Average total size (kbytes): 0\
        Maximum resident set size (kbytes): 47960\
        Average resident set size (kbytes): 0\
        Major (requiring I/O) page faults: 0\
        Minor (reclaiming a frame) page faults: 11602\
        Voluntary context switches: 38468\
        Involuntary context switches: 158\
        Swaps: 0\
        File system inputs: 0\
        File system outputs: 0\
        Socket messages sent: 0\
        Socket messages received: 0\
        Signals delivered: 0\
        Page size (bytes): 4096\
        Exit status: 1\
*/

/*\
With 100 Threads, 3000 Allocations of each 1024 kBit\
Command being timed: "./membench 100 3000 1024"\
        User time (seconds): 0.36\
        System time (seconds): 1.29\
        Percent of CPU this job got: 79%\
        Elapsed (wall clock) time (h:mm:ss or m:ss): 0:02.08\
        Average shared text size (kbytes): 0\
        Average unshared data size (kbytes): 0\
        Average stack size (kbytes): 0\
        Average total size (kbytes): 0\
        Maximum resident set size (kbytes): 228236\
        Average resident set size (kbytes): 0\
        Major (requiring I/O) page faults: 0\
        Minor (reclaiming a frame) page faults: 74367\
        Voluntary context switches: 266295\
        Involuntary context switches: 379\
        Swaps: 0\
        File system inputs: 0\
        File system outputs: 0\
        Socket messages sent: 0\
        Socket messages received: 0\
        Signals delivered: 0\
        Page size (bytes): 4096\
        Exit status: 0\
*/
#define _POSIX_SOURCE
#define _GNU_SOURCE
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <pthread.h>

typedef struct memory
{
    long size;
    long allocs;
} memory_t;

memory_t *memory;

void *thread_caller(void *arg)
{
    int *pointer_arr[memory->allocs];
    for (int i = 0; i < memory->allocs; i++) {
        pointer_arr[i] = malloc(memory->size);
        fprintf(stdout, "Thread [%d] -> alloc %d done\n", *(int *)arg, i);
        if (pointer_arr[i] == 0) {
            perror("malloc");
            exit(EXIT_FAILURE);
        }
    }

    for (int i = 0; i < memory->allocs; i++) {
        free(pointer_arr[i]);
    }

    pthread_exit(NULL);
}

int main(int argc, char const *argv[])
{
    if (argc < 4) {
        printf("You must specify at least 4 arguments!");
        exit(EXIT_FAILURE);
    } else {
        long THREAD_NUMBER;
        int rc;
        memory = malloc(sizeof(*memory));
        THREAD_NUMBER = strtol(argv[1], NULL, 10);
        memory->allocs = strtol(argv[2], NULL, 10);
        memory->size = strtol(argv[3], NULL, 10);
        pthread_t threads[THREAD_NUMBER];
        for (int i = 0; i < THREAD_NUMBER; i++) {
            if ((rc = pthread_create(&threads[i], NULL, &thread_caller, (void *)&i)) < 0) {
                perror("pthread_create");
                exit(EXIT_FAILURE);
            }
        }

        for (int i = 0; i < THREAD_NUMBER; i++) {
            if (pthread_join(threads[i], NULL) < 0) {
                perror("pthread_join");
                exit(EXIT_FAILURE);
            }
        }
    }
    free(memory);
    memory = NULL;
    return EXIT_SUCCESS;
}

/*
With 100 Threads, 1000 Allocations of each 1024 kBit
Command exited with non-zero status 1
        Command being timed: "./global_allocator 100 1000 1024"
        User time (seconds): 0.03
        System time (seconds): 0.19
        Percent of CPU this job got: 72%
        Elapsed (wall clock) time (h:mm:ss or m:ss): 0:00.31
        Average shared text size (kbytes): 0
        Average unshared data size (kbytes): 0
        Average stack size (kbytes): 0
        Average total size (kbytes): 0
        Maximum resident set size (kbytes): 47960
        Average resident set size (kbytes): 0
        Major (requiring I/O) page faults: 0
        Minor (reclaiming a frame) page faults: 11602
        Voluntary context switches: 38468
        Involuntary context switches: 158
        Swaps: 0
        File system inputs: 0
        File system outputs: 0
        Socket messages sent: 0
        Socket messages received: 0
        Signals delivered: 0
        Page size (bytes): 4096
        Exit status: 1
*/

/*
With 100 Threads, 3000 Allocations of each 1024 kBit
Command being timed: "./membench 100 3000 1024"
        User time (seconds): 0.36
        System time (seconds): 1.29
        Percent of CPU this job got: 79%
        Elapsed (wall clock) time (h:mm:ss or m:ss): 0:02.08
        Average shared text size (kbytes): 0
        Average unshared data size (kbytes): 0
        Average stack size (kbytes): 0
        Average total size (kbytes): 0
        Maximum resident set size (kbytes): 228236
        Average resident set size (kbytes): 0
        Major (requiring I/O) page faults: 0
        Minor (reclaiming a frame) page faults: 74367
        Voluntary context switches: 266295
        Involuntary context switches: 379
        Swaps: 0
        File system inputs: 0
        File system outputs: 0
        Socket messages sent: 0
        Socket messages received: 0
        Signals delivered: 0
        Page size (bytes): 4096
        Exit status: 0
*/