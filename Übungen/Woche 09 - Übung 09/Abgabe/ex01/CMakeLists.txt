set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -std=gnu11 -Wall -Werror")

add_executable(w09_ex01_membench membench.c)
target_link_libraries(w09_ex01_membench pthread)
