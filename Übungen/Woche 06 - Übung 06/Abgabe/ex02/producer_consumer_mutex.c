#include "general_pthread.h"
#include "myqueue.h"

pthread_mutex_t pthread_mutex;

void printQueue() {
    printf("length: %u", size());
    if (!empty()) {
        printf(", front: %u", front());
    }
    printf("\n");
}

void *consume() {
    unsigned int sum = 0, result = 0;
    long tid = pthread_self();
    while (true) {
        if (pthread_mutex_lock(&pthread_mutex) != 0) {
            error_msg("pthread_mutex_lock");
        }
        if (!empty()) {
            result = front();
            pop();
            if (result == 1) {
                sum += result;
            } else {
                if (pthread_mutex_unlock(&pthread_mutex) != 0) {
                    error_msg("pthread_mutex_unlock");
                }
                break;
            }
        }
        if (pthread_mutex_unlock(&pthread_mutex) != 0) {
            error_msg("pthread_mutex_unlock");
        }
    }
    printf("Thread-ID %ld: %u\n", tid, sum);
    pthread_exit(NULL);
}

void produce() {
    for (int i = 0; i < ITERATIONS; i++) {
        // To ensure that while producing consumer can already consume
        if (pthread_mutex_lock(&pthread_mutex) != 0) {
            error_msg("pthread_mutex_lock");
        }
        push(1);
        if (pthread_mutex_unlock(&pthread_mutex) != 0) {
            error_msg("pthread_mutex_unlock");
        }
    }

    for (int j = 0; j < THREAD_NUMBER; j++) {
        if (pthread_mutex_lock(&pthread_mutex) != 0) {
            error_msg("pthread_mutex_lock");
        }
        push(0);
        if (pthread_mutex_unlock(&pthread_mutex) != 0) {
            error_msg("pthread_mutex_unlock");
        }
    }
}

int main(int argc, char const *argv[]) {
    // Create queue
    create();
    pthread_t threads[THREAD_NUMBER];

    // Initialize pthread mutex
    if (pthread_mutex_init(&pthread_mutex, NULL) != 0) {
        error_msg("pthread_mutex_init");
    }

    // Create THREAD_NUMBER threads and call consume function for each thread when created
    for (long i = 0; i < THREAD_NUMBER; i++) {
        if (pthread_create(&threads[i], NULL, consume, (void *) &i) < 0) {
            error_msg("pthread_create");
        }
    }

    // Feed with entries in queue
    produce();

    // Wait for all threads to stop
    for (int i = 0; i < THREAD_NUMBER; i++) {
        if (pthread_join(threads[i], NULL) != 0) {
            error_msg("pthread_join");
        }
    }

    // Destroy mutex
    if (pthread_mutex_destroy(&pthread_mutex) != 0) {
        error_msg("pthread_mutex_destroy");
    }

    return EXIT_SUCCESS;
}
