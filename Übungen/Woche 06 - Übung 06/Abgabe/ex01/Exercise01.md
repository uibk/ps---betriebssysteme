##### Differences between syscall(SYS_gettid) and pthread_self()

###### syscall

- syscall id's uses same data type than pid_t (process id's)
- they are system-wide unique so no thread can have same id (kernel guarantees that)
- mostly not portable

###### pthread_self()

- pthread_equal function is needed to compare pthread id's
- POSIX (Linux) unique identifiers
- maintained and assigned by the threading implementation
- unique withing the application

portable if usage is like this:

```C
pthread_t my_tid; //filled elsewhere

pthread_t tid = pthread_self();

if( pthread_equal(my_tid, tid) ) {
   /* do stuff */
}
```