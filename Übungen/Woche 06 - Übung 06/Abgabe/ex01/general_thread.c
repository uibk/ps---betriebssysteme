#include "general_pthread.h"

void cleanup_handler(void *arg) {
    if (fclose((FILE *) arg) < 0) {
        error_msg("fclose");
    }
}

void *write_file(void *tid) {
    sleep(rand() % 4);

    // Allow to disable canceling current thread
    if (pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, NULL) != 0) {
        error_msg_thread("pthread_cancelstate");
    }

    char filename_buf[sizeof "threadN.txt"];

    snprintf(filename_buf, sizeof(filename_buf) + 1, "thread%d.txt", *((int *) tid));

    FILE *file;

    if ((file = fopen(filename_buf, "w")) == NULL) {
        error_msg_thread("fopen");
    }

    // Add a clean up handler as thread has opened the file
    pthread_cleanup_push(cleanup_handler, file)

            // Thread can be canceled again
            if (pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL) != 0) {
                error_msg_thread("pthread_cancelstate");
            }

            // Write to file
            fprintf(file, "%ld\n", pthread_self());
            //
    pthread_cleanup_pop(1);
    pthread_exit(NULL);
}

int main(int argc, char const *argv[]) {
    srand(time(NULL));
    pthread_t threads[THREAD_NUMBER];
    int thread_num[THREAD_NUMBER];

    for (int i = 0; i < THREAD_NUMBER; i++) {
        thread_num[i] = i;
        if ((pthread_create(&threads[i], NULL, &write_file, (void *) &thread_num[i])) < 0) {
            error_msg("pthread_cancel");
        }
    }

    for (int i = 0; i < THREAD_NUMBER; i++) {
        // Only cancel thread when number 1 was generated => 50% chance
        if ((rand() % 2)) {
            fprintf(stdout, "Thread %ld got cancelled..\n", pthread_self());
            if (pthread_cancel(threads[i]) < 0) {
                error_msg("pthread_cancel");
            }
        }
    }

    // Wait for all threads to stop
    for (int i = 0; i < THREAD_NUMBER; i++) {
        if (pthread_join(threads[i], NULL) < 0) {
            error_msg("pthread_join");
        }
    }

    return EXIT_SUCCESS;
}
