##### Philosopher's dining Problem

- there are 5 philosophers who wanna to eat
- each philosopher has a right and left chopstick
- a philosopher can only eat when he has both chopsticks 
- each time a philosopher get both sticks he eats some time and then release the chopsticks again

###### Deadlock

- the deadlock occur when a philosopher want's to get a chopstick, but the chopstick is used by another philosopher (which also waits for the other chopstick) -> they are waiting forever to get the another chopstick and never terminate
- it can happen that it successfully runs for 5 runs but on the 6th run it hangs. It always depends on the order of the releasing chopsticks. When every philosopher gets the chopsticks at the right time and in the right order, no deadlocks are present.
  
###### Solution

- one approach (like submitted) is to use another mutex to lock the eating process until the chopsticks can be released again to prevent that another thread or philosopher wants to get the stick at the same time.
- this cannot result in a deadlock as every philosopher is "locked" when he is in use of his chopsticks, no other thread can disturbe this operation. When the operation is done and the chopsticks are released again, every one can gain or access the resource again. The deadlock only occur on accessing or grabbing the chopsticks, but not when releasing it. 