#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include <math.h>
#include <pthread.h>
#include <stdbool.h>

#define NUMBER 21

typedef struct node
{
    struct node *next;
    struct node *free_list;
    bool used;
} Node;

Node *list_array[NUMBER];
pthread_mutex_t mutex;

Node *init_free_list()
{
    Node *node;
    if ((node = (Node *)malloc(sizeof(Node))) == NULL)
    {
        perror("malloc");
        return NULL;
    }
    node->next = NULL;
    node->free_list = node;
    node->used = true;

    return node;
}

Node *insert_to_list(Node *start_node, size_t size)
{
    Node *current_node = start_node;
    Node *prev_node = NULL;
    while (current_node != NULL && current_node->used)
    {
        prev_node = current_node;
        current_node = current_node->next;
    }

    if (current_node == NULL)
    {
        if ((current_node = malloc(sizeof(Node) + size)) == NULL)
        {
            return NULL;
        }
    }
    current_node->next = NULL;
    current_node->free_list = start_node;
    current_node->used = true;
    prev_node->next = current_node;
    return current_node;
}

void init_allocator()
{
    for (int i = 0; i < NUMBER; i++)
    {
        if ((list_array[i] = init_free_list()) == NULL)
        {
            perror("allocation failed!");
            exit(EXIT_FAILURE);
        }
    }

    pthread_mutex_init(&mutex, NULL);
}

void free_list(Node *start_node)
{
    Node *current_node = start_node;
    Node *next_node = NULL;

    while (current_node != NULL)
    {
        next_node = current_node->next;
        free(current_node);
        current_node = next_node;
    }
}

void free_allocator()
{
    for (int i = 0; i < NUMBER; i++)
    {
        free_list(list_array[i]);
    }
    pthread_mutex_destroy(&mutex);
}

void *my_malloc(size_t size)
{
    int current_power = 0;
    int i = 1;
    Node *new_node = NULL;
    for (; i < NUMBER - 1; i++)
    {
        current_power = pow(2, i);
        if (size <= current_power)
        {
            break;
        }
    }
    pthread_mutex_lock(&mutex);
    if ((new_node = insert_to_list(list_array[i], size)) == NULL)
    {
        perror("insert to list not possible!");
        return NULL;
    }
    pthread_mutex_unlock(&mutex);

    return (void *)new_node + sizeof(Node);
}

int used_false(Node *list_start_node, Node *delete_node)
{
    Node *current_node = list_start_node;

    while (current_node != NULL && current_node != delete_node)
    {
        current_node = current_node->next;
    }

    // If no node has been found return 0 as failure
    if (current_node == NULL)
    {
        return 0;
    }

    current_node->used = false;
    return 1;
}

void my_free(void *adress)
{
    Node *free_node = adress - sizeof(Node);

    int i = 0;
    for (i = 0; i <= NUMBER; i++)
    {
        if (list_array[i] == free_node->free_list)
        {
            break;
        }
    }

    if (i == NUMBER)
    {
        printf("Wrong pointer passed!");
        return;
    }

    pthread_mutex_lock(&mutex);
    if (used_false(free_node->free_list, free_node) == 0)
    {
        perror("Wrong pointer!\n");
    }
    pthread_mutex_unlock(&mutex);
}