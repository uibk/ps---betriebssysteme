
#define _POSIX_SOURCE
#define _GNU_SOURCE

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <time.h>
#include "global_freelist_allocator.h"

typedef struct memory {
    long size;
    long allocs;
} memory_t;

memory_t *memory;
unsigned int seed;

void *thread_caller(void *arg) {
    int memory_size = rand_r(&seed) % ((8 * memory->size + 1 - memory->size)) + memory->size;
    int *pointer_arr[memory->allocs];
    for (int i = 0; i < memory->allocs; i++) {
        pointer_arr[i] = my_malloc(memory_size);
        // fprintf(stdout, "Thread [%d] -> alloc %d of size %ld done\n", *(int *)arg, i, memory->size);
        if (pointer_arr[i] == 0) {
            perror("malloc");
            exit(EXIT_FAILURE);
        }
    }
    for (int i = 0; i < memory->allocs; i++) {
        if (rand() % 2 == 1) {
            my_free(pointer_arr[i]);
            pointer_arr[i] = NULL;
        }
    }
    int memory_size2 = rand_r(&seed) % ((8 * memory->size + 1 - memory->size)) + memory->size;
    for (int i = 0; i < (memory->allocs / 2); i++) {
        pointer_arr[i] = my_malloc(memory_size2);
        // fprintf(stdout, "Thread [%d] -> alloc %d of size %ld done\n", *(int *)arg, i, memory->size);
        if (pointer_arr[i] == 0) {
            perror("malloc");
            exit(EXIT_FAILURE);
        }
    }
    for (int i = 0; i < memory->allocs; i++) {
        if (pointer_arr[i] != NULL) {
            my_free(pointer_arr[i]);
        }
    }

    pthread_exit(NULL);
}

int main(int argc, char const *argv[]) {
    if (argc < 4) {
        printf("You must specify at least 4 arguments!");
        exit(EXIT_FAILURE);
    } else {
        init_allocator();
        srand(time(NULL));
        seed = time(NULL);
        long THREAD_NUMBER;
        int rc;
        memory = malloc(sizeof(*memory));
        THREAD_NUMBER = strtol(argv[1], NULL, 10);
        memory->allocs = strtol(argv[2], NULL, 10);
        memory->size = strtol(argv[3], NULL, 10);
        pthread_t threads[THREAD_NUMBER];
        for (int i = 0; i < THREAD_NUMBER; i++) {
            if ((rc = pthread_create(&threads[i], NULL, &thread_caller, (void *) &i)) < 0) {
                perror("pthread_create");
                exit(EXIT_FAILURE);
            }
        }

        for (int i = 0; i < THREAD_NUMBER; i++) {
            if (pthread_join(threads[i], NULL) < 0) {
                perror("pthread_join");
                exit(EXIT_FAILURE);
            }
        }
    }
    free(memory);
    memory = NULL;
    return EXIT_SUCCESS;
}
