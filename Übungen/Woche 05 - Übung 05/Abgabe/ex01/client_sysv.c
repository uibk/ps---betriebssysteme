#include "general_sysv.h"

int main(int argc, char const *argv[]) {
    int shmid, *counter;

    fprintf(stdout, "%d: accessing shared memory segment...", getpid());

    // Create or get existing shared memory
    if ((shmid = shmget(SHM_KEY, SHM_SZ, IPC_CREAT | 0666)) < 0) {
        error_msg("shmget")
    }

    // Attach shared memory to address space of parent process
    if ((counter = shmat(shmid, NULL, 0)) < 0) {
        error_msg("shmat")
    }

    fprintf(stdout, "\n%d: creating %d children...", getpid(), PROCESSES);

    for (int i = 0; i < PROCESSES; i++) {
        switch (fork()) {
            case -1:
            error_msg("fork")
            case 0:
                for (int j = 0; j < ITERATIONS; j++) {
                    (*counter)++;
                }
                _exit(EXIT_SUCCESS);
        }
    }

    fprintf(stdout, "\n%d: waiting for children...", getpid());

    // Wait for all child processes to terminate
    while (wait(NULL) > 0);

    fprintf(stdout, "\n%d: printing to stdout... result = %d", getpid(), *counter);

    fprintf(stdout, "\n%d: writing to fifo... ", getpid());

    FILE *fifo_fp;

    // Open FIFO in write mode
    if ((fifo_fp = fopen(FIFO_NAME, "w")) == NULL) {
        error_msg("fopen")
    }

    // Write value of counter to FIFO
    fprintf(fifo_fp, "%d", *counter);

    // Close fifo
    if (fclose(fifo_fp) < 0) {
        error_msg("fclose")
    }

    // Detach shared memory segment
    if (shmdt(counter) < 0) {
        error_msg("shmdt")
    }

    // Delete shared memory segment
    if (shmctl(shmid, IPC_RMID, 0) < 0) {
        error_msg("shmctl")
    }
    fprintf(stdout, "done\n");

    return EXIT_SUCCESS;
}
