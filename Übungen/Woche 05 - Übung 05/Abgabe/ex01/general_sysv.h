#ifndef PS_BETRIEBSSYSTEME_GENERAL_SYSV_H
#define PS_BETRIEBSSYSTEME_GENERAL_SYSV_H

#define _POSIX_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/shm.h>
#include <sys/ipc.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/sem.h>
#include <fcntl.h>

#define FIFO_NAME "RESULT_FIFO"
#define SHM_SZ 16
#define SHM_KEY 1234
#define ITERATIONS 10000
#define PROCESSES 1000
#define SEM_NUMBER 1

#define error_msg(msg) \
    perror(msg);       \
    _exit(EXIT_FAILURE);

#endif //PS_BETRIEBSSYSTEME_GENERAL_SYSV_H
