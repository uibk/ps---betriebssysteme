cmake_minimum_required(VERSION 3.14)
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -std=gnu11 -Wall -Werror")

add_executable(w05_ex01_client client_sysv.c general_sysv.h)
add_executable(w05_ex01_server server_sysv.c general_sysv.h)
target_link_libraries(w05_ex01_client rt pthread)
target_link_libraries(w05_ex01_server rt pthread)
