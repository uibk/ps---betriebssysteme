#include "general_sysv.h"

int main(int argc, char const *argv[]) {
    int shmid, *counter;

    fprintf(stdout, "%d: creating shared memory segment... ", getpid());

    if ((shmid = shmget(SHM_KEY, SHM_SZ, IPC_CREAT | 0666)) < 0) {
        error_msg("shmget")
    }

    if ((counter = shmat(shmid, NULL, 0)) < 0) {
        error_msg("shmat")
    }

    *counter = 0;

    fprintf(stdout, "done\n%d: creating fifo... ", getpid());

    FILE *fifo_fp;

    unlink(FIFO_NAME);

    if (mkfifo(FIFO_NAME, 0777) == -1) {
        error_msg("mkfifo")
    }

    // Open FIFO with Read-only mode
    if ((fifo_fp = fopen(FIFO_NAME, "r")) == NULL) {
        error_msg("fopen")
    }

    char buf[SHM_SZ], *end;
    if (read(fileno(fifo_fp), buf, sizeof(buf)) > 0) {
        fprintf(stdout, "\n%d: received: %ld\n", getpid(), strtol(buf, &end, 10));
    }

    if (fclose(fifo_fp) < 0) {
        error_msg("fclose");
    }

    if (unlink(FIFO_NAME) == -1) {
        error_msg("unlink");
    }

    if (shmdt(counter) < 0) {
        error_msg("shmdt");
    }

    return EXIT_SUCCESS;
}