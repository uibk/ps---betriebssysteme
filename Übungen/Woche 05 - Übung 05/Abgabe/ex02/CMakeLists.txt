cmake_minimum_required(VERSION 3.14)
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -std=gnu11 -Wall -Werror")

add_executable(w05_ex02_client_sysv client_sysv_semaphore.c)
add_executable(w05_ex02_server_sysv server_sysv_semaphore.c)
target_link_libraries(w05_ex02_client_sysv rt pthread)
target_link_libraries(w05_ex02_server_sysv rt pthread)
