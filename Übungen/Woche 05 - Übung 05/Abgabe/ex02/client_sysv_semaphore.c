#include "general_sysv.h"

int main(int argc, char const *argv[]) {
    int shmid, *counter;
    fprintf(stdout, "%d: accessing shared memory segment...", getpid());

    // Get shared memory or create it
    if ((shmid = shmget(SHM_KEY, SHM_SZ, IPC_CREAT | 0666)) < 0) {
        error_msg("shmget")
    }

    // Attach shared memory segment
    if ((counter = shmat(shmid, NULL, 0)) < 0) {
        error_msg("shmat")
    }

    fprintf(stdout, "\n%d: getting semaphore...", getpid());

    // Get semaphore
    int sem_id;
    if ((sem_id = semget(SEM_KEY, SEM_NUMBER, 0666)) < 0) {
        error_msg("semget")
    }

    // Init semaphore with operation flag -1
    struct sembuf sb;
    sb.sem_num = 0;
    sb.sem_op = -1;
    sb.sem_flg = 0;

    for (int i = 0; i < PROCESSES; i++) {
        switch (fork()) {
            case -1:
            error_msg("fork")
            case 0:
                // Decrement semaphore for blocking and entering critical region
                if (semop(sem_id, &sb, 1) < 0) {
                    error_msg("semop")
                }

                // Increment counter variable
                for (int j = 0; j < ITERATIONS; j++) {
                    (*counter)++;
                }
                // Set operation to 1 for ???
                sb.sem_op = 1;

                // Increment to unlock semaphore again??
                if (semop(sem_id, &sb, 1) < 0) {
                    error_msg("semop")
                }
                _exit(EXIT_SUCCESS);
        }
    }
    fprintf(stdout, "\n%d: waiting for children...", getpid());

    // Wait for all children to terminate
    while (wait(NULL) > 0);

    fprintf(stdout, "\n%d: printing to stdout... result = %d", getpid(), *counter);

    fprintf(stdout, "\n%d: writing to fifo... ", getpid());
    FILE *fifo_fp;

    if ((fifo_fp = fopen(FIFO_NAME, "w")) == NULL) {
        error_msg("fopen")
    }

    fprintf(fifo_fp, "%d", *counter);

    if (fclose(fifo_fp) < 0) {
        error_msg("fclose")
    }

    if (shmdt(counter) < 0) {
        error_msg("shmdt")
    }

    if (shmctl(shmid, IPC_RMID, 0) < 0) {
        error_msg("shmctl")
    }

    fprintf(stdout, "done\n");

    return EXIT_SUCCESS;
}
