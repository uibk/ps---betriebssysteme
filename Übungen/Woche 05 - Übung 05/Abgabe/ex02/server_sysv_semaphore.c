#include "general_sysv.h"

union semun {
    int val;
    struct semid_ds *buf;
    short *array;
};

int main(int argc, char const *argv[]) {
    int shm_id, *counter;
    int sem_id;
    union semun arg;

    if ((sem_id = semget(SEM_KEY, SEM_NUMBER, 0666 | IPC_CREAT)) < 0) {
        perror("semget");
        exit(EXIT_FAILURE);
    }

    //TODO: explane purpose of this statement
    arg.val = 1;

    if (semctl(sem_id, 0, SETVAL, arg) < 0) {
        perror("semctl");
        exit(EXIT_FAILURE);
    }

    fprintf(stdout, "%d: creating shared memory segment... ", getpid());

    if ((shm_id = shmget(SHM_KEY, SHM_SZ, IPC_CREAT | 0666)) < 0) {
        perror("shmget");
        exit(EXIT_FAILURE);
    }

    if ((counter = shmat(shm_id, NULL, 0)) < 0) {
        perror("shmat");
        exit(EXIT_FAILURE);
    }

    *counter = 0;

    fprintf(stdout, "done\n%d: creating fifo... ", getpid());

    FILE *fifo_fp;

    // Deletes name from filesystem and (if no process is using file anymore) space will be free'd of the file
    unlink(FIFO_NAME);

    // Create FIFO
    if (mkfifo(FIFO_NAME, 0777) == -1) {
        perror("mkfifo");
        return EXIT_FAILURE;
    }

    // Open FIFO in read mode
    if ((fifo_fp = fopen(FIFO_NAME, "r")) == NULL) {
        perror("fopen");
        return EXIT_FAILURE;
    }

    char buf[SHM_SZ], *end;
    if (read(fileno(fifo_fp), buf, sizeof(buf)) > 0) {
        fprintf(stdout, "\n%d: received: %ld\n", getpid(), strtol(buf, &end, 10));
    }

    if (fclose(fifo_fp) < 0) {
        perror("fclose");
        return EXIT_FAILURE;
    }

    if (unlink(FIFO_NAME) == -1) {
        perror("unlink");
        return EXIT_FAILURE;
    }

    if (shmdt(counter) < 0) {
        perror("shmdt");
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}