#### POSIX and System-V comparison

- in System-V you can control how much the semaphore count can be increased or decreased respectively. POSIX only in- and decreases by 1.
- POSIX not allows to manipulte semaphore permissions. In System V you are allowed to change permissions of semaphores to a subset of the orginally set permission.
- POSIX init and creation is atomic
- scalability of POSIX semaphores are way higher than System V
- System V creates array of semaphores whereas POSIX just creates one -> costs more
- Better performance from POSIX to System V
- POSIX provide mechanism for just process-wide semaphores. Is safer.
- POSIX does not provide functionality to wake up waiting process when other process died which was mutexed. -> can lead to zombie processes
- in System V you can use *ipcs* and *ipcrm* -S (Semaphore) or -M (Shared Memory) list all semaphores and shared memories and delete them.
- POSIX uses file semantics for semaphores and shared memory. Therefore, different objects could have the same name which causes in undefined behaviour of each acting process.
- As mentioned above, POSIX cannot send signals to processes.
- System V has the ability to acquire the semaphore resource in a way which is automatically returned by the kernel. However, it does not depend on how the process exits. 