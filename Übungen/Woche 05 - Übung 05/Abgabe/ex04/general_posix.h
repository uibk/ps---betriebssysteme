#ifndef PS_BETRIEBSSYSTEME_GENERAL_POSIX_H
#define PS_BETRIEBSSYSTEME_GENERAL_POSIX_H
// When including errno.h no need to include POSIX_SOURCE anymore
//#define _POSIX_SOURCE
#define _GNU_SOURCE

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/shm.h>
#include <semaphore.h>
#include <fcntl.h>
#include <time.h>
#include <wait.h>
#include <string.h>
#include <errno.h>

#define SHM_SZ 16
#define SHM_NAME "SHM"
#define SEM_NAME "SEM"
#define FIFO_NAME "RESULT_FIFO"
#define SEM_NUMBER 1
#define PROCESSES 1000
#define ITERATIONS 10000

#define error_msg(msg) \
    perror(msg);       \
    _exit(EXIT_FAILURE);

#endif //PS_BETRIEBSSYSTEME_GENERAL_POSIX_H
