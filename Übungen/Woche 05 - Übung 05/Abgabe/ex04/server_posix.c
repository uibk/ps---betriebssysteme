#include "general_posix.h"

int main(int argc, char const *argv[]) {
    int shm_fd, *shm_ptr;

    fprintf(stdout, "%d: creating shared memory segment...", getpid());

    if ((shm_fd = shm_open(SHM_NAME, O_CREAT | O_RDWR, 0666)) < 0) {
        error_msg("shm_open")
    }

    if (ftruncate(shm_fd, SHM_SZ) < 0) {
        error_msg("ftruncate")
    }

    if ((shm_ptr = mmap(0, SHM_SZ, PROT_WRITE, MAP_SHARED, shm_fd, 0)) == (void *) -1) {
        error_msg("mmap")
    }

    *shm_ptr = 0;

    fprintf(stdout, "done\n%d: creating fifo... ", getpid());

    FILE *fifo_fp;

    if (mkfifo(FIFO_NAME, 0666) < 0) {
        if (errno != EEXIST) {
            error_msg("mkfifo")
        }
    }

    if ((fifo_fp = fopen(FIFO_NAME, "r")) == NULL) {
        error_msg("fopen")
    }

    char buf[SHM_SZ], *end;
    // If read from FIFO File stream is successful, write it to stdout
    if (read(fileno(fifo_fp), buf, sizeof(buf)) > 0) {
        fprintf(stdout, "\n%d: received: %ld\n", getpid(), strtol(buf, &end, 10));
    }

    if (fclose(fifo_fp) < 0) {
        error_msg("fclose")
    }

    // Release FIFO after reading from it
    unlink(FIFO_NAME);

    // Unmap attached shared memory segment
    if (munmap(shm_ptr, SHM_SZ) < 0) {
        error_msg("munmap")
    }

    // Delete shared memory
    if (shm_unlink(SHM_NAME) < 0) {
        error_msg("shm_unlink")
    }

    return EXIT_SUCCESS;
}