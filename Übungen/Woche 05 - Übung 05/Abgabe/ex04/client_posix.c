#include "general_posix.h"

sem_t *mutex;

int main(int argc, char const *argv[]) {
    int shm_fd, *shm_ptr;

    fprintf(stdout, "%d: accessing shared memory segment...", getpid());

    // Create Shared memory segment
    if ((shm_fd = shm_open(SHM_NAME, O_RDWR | O_CREAT, S_IRWXU | S_IRWXO | S_IRWXG)) < 0) {
        error_msg("shm_open")
    }

    // Adjust right size of shared memory
    if (ftruncate(shm_fd, SHM_SZ) < 0) {
        error_msg("ftruncate")
    }

    // Map shared memory to own process address space
    if ((shm_ptr = mmap(NULL, SHM_SZ, PROT_WRITE, MAP_SHARED, shm_fd, 0)) == (void *) -1) {
        error_msg("mmap")
    }

    fprintf(stdout, "%d: creating semaphore...", getpid());

    // Create POSIX semaphore
    if ((mutex = sem_open(SEM_NAME, O_CREAT, 0666, 1)) == SEM_FAILED) {
        error_msg("sem_open")
    }

    fprintf(stdout, "done\n%d: creating %d children...", getpid(), PROCESSES);

    for (int i = 0; i < PROCESSES; i++) {
        switch (fork()) {
            case -1:
            error_msg("fork")
            case 0:
                for (int j = 0; j < ITERATIONS; j++) {
                    // Lock with mutex and enter critical region
                    sem_wait(mutex);

                    (*shm_ptr)++;

                    // Release mutex
                    sem_post(mutex);
                }
                _exit(EXIT_SUCCESS);
        }
    }

    fprintf(stdout, "\n%d: waiting for children...", getpid());

    // Wait for all children
    while (wait(NULL) > 0);

    // Free semaphore
    if (sem_unlink(SEM_NAME) < 0) {
        error_msg("sem_unlink")
    }

    // Close semaphore
    if (sem_close(mutex) < 0) {
        error_msg("sem_close")
    }

    fprintf(stdout, "\n%d: printing to stdout... result = %d", getpid(), *(int *) shm_ptr);

    fprintf(stdout, "\n%d: writing to fifo... ", getpid());

    FILE *fifo_fp;
    // Open FIFO in read mode
    if ((fifo_fp = fopen(FIFO_NAME, "w")) == NULL) {
        error_msg("fopen")
    }

    // Write shared memory value to FIFO
    fprintf(fifo_fp, "%d", *shm_ptr);

    // Close FIFO
    if (fclose(fifo_fp) < 0) {
        error_msg("fclose")
    }

    // Unmap attached shared memory segment
    if (munmap(shm_ptr, SHM_SZ) < 0) {
        error_msg("munmap")
    }

    // Free shared memory
    if (shm_unlink(SHM_NAME) < 0) {
        error_msg("shm_unlink")
    }

    fprintf(stdout, "done\n");

    return EXIT_SUCCESS;
}
