#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <time.h>
#include <sys/ipc.h>
#include <sys/msg.h>

#define MSG_LEN 1024
#define PIPE_BUF 4096
#define KEY 1234

typedef struct {
    long message_type;
    char message[MSG_LEN];
} msg_buffer_t;

int get_msq(key_t key, int flag) {
    int res;
    res = msgget(key, flag);
    if (res == -1) {
        perror("msgget()");
        return -1;
    }
    return res;
}

int main(int argc, char const *argv[]) {
    msg_buffer_t msg_buf;
    msg_buf.message_type = 1;
    int res, msq_id;
    msq_id = get_msq(KEY, 0666 | IPC_CREAT);
    if (msq_id < 0) {
        exit(EXIT_FAILURE);
    }
    int i = 1;
    srand(time(NULL));
    int random;
    while (1) {
        random = rand() % 8;
        sprintf(msg_buf.message, "%s%d%s", "[middleware] message ", i, " from middleware");
        if (strlen(msg_buf.message) < PIPE_BUF) {

            res = msgsnd(msq_id, &msg_buf, MSG_LEN, 0);
        }
        if (res == -1) {
            perror("msgsnd()");
            exit(EXIT_FAILURE);
        }
        sleep(random);
        i++;
    }
    return EXIT_SUCCESS;
}
