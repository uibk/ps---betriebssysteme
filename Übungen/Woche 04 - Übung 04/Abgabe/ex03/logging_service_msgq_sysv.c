#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/select.h>
#include <unistd.h>
#include <errno.h>
#include <sys/msg.h>
#include <sys/ipc.h>

#define MSG_LEN 1024
#define PIPE_BUF 4096
#define KEY 1234

void error_msg(char *msg) {
    perror(msg);
    exit(EXIT_FAILURE);
}

int get_msq(key_t key, int flag) {
    int res;
    res = msgget(key, flag);
    if (res < 0) {
        printf("Error getting message queue with key %d", key);
        return -1;
    }
    return res;
}

typedef struct {
    long priority;
    char message[MSG_LEN];
} msg_buffer_t;

int main(int argc, char const *argv[]) {
    msg_buffer_t msg_buf;
    int msq_id = get_msq(KEY, 0666 | IPC_CREAT);
    if (msq_id < 0) {
        error_msg("msgget");
    }
    while (1) {
        int res = msgrcv(msq_id, &msg_buf, MSG_LEN, 1, 0);
        if (res < 0) {
            error_msg("msgcrv");
        }
        fprintf(stdout, "%s\n", msg_buf.message);
    }
}
