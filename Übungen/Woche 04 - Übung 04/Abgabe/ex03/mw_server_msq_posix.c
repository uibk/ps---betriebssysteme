#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <time.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <mqueue.h>

#define MSG_LEN 1024
#define PIPE_BUF 4096
#define MSQ_NAME "/MSQ_NAME\0"

mqd_t sender_msq;

void error_msg(char *msg) {
    perror(msg);
    exit(EXIT_FAILURE);
}

int main(int argc, char const *argv[]) {

    // Random generator initialization
    srand(time(NULL));
    int random;
    // Open message queue in write only mode
    if ((sender_msq = mq_open(MSQ_NAME, O_WRONLY)) < 0) {
        error_msg("mq_open");
    }
    int i = 0;
    random = rand() % 6 + 2;
    while (1) {
        char message[MSG_LEN];
        // Write in message buffer
        sprintf(message, "%s", "[middleware] message");
        if (strlen(message) < PIPE_BUF) {
            // Send message to message queue
            if (mq_send(sender_msq, message, strlen(message) + 1, 1) < 0) {
                error_msg("mq_send");
            }
        }
        // Sleep between 0 and 7 seconds
        sleep(random);
        i++;
    }
}
