#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/select.h>
#include <unistd.h>
#include <errno.h>
#include <sys/msg.h>
#include <sys/ipc.h>
#include <mqueue.h>

#define MSG_LEN 1024
#define MSQ_NAME "/MSQ_NAME\0"

void error_msg(char *msg) {
    perror(msg);
    exit(EXIT_FAILURE);
}

mqd_t receiver_msq;

int main(int argc, char const *argv[]) {
    struct mq_attr attribute;
    attribute.mq_flags = 0;
    attribute.mq_maxmsg = 10;
    attribute.mq_msgsize = 200;

    char recv_msg[MSG_LEN];

    // Create message queue
    if ((receiver_msq = mq_open(MSQ_NAME, O_CREAT, 0644, &attribute)) < 0) {
        error_msg("mq_open");
    }

//    // Open message queue in read only mode
//    if ((receiver_msq = mq_open(MSQ_NAME, O_RDONLY)) < 0) {
//        error_msg("mq_open");
//    }
    while (1) {
        // Receive message from message queue
        if (mq_receive(receiver_msq, recv_msg, MSG_LEN, NULL) < 0) {
            error_msg("mq_receive");
        }
        // Read from message queue
        fprintf(stdout, "%s\n", recv_msg);
    }
}
