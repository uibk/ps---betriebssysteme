set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -std=gnu11 -Wall -Werror")

add_executable(w04_ex02_ls_grep ls_grep.c)