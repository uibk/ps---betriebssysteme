#include <string.h>
#include <wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <fcntl.h>

int main(int argc, char const *argv[]) {
    int fd[2];
    pid_t grep, ls;

    if (pipe(fd) == -1) {
        perror("pipe()");
        return EXIT_FAILURE;
    }
    ls = fork();
    grep = fork();

    if (ls == -1 || grep == -1) {
        perror("fork()");
        return EXIT_FAILURE;
    }
    if (ls == 0) {
        close(fd[0]);               // close read end of pipe
        dup2(fd[1], STDOUT_FILENO); // redirect stdout to pipe
        close(fd[1]);               // close write end of pipe
        if ((execlp("ls", "ls", NULL)) < 0) {
            perror("exelpc");
            exit(EXIT_FAILURE);
        }
        exit(EXIT_SUCCESS);
    }
    if (grep == 0) {
        close(fd[1]);              // close write end of pipe
        dup2(fd[0], STDIN_FILENO); // redirect stdin to pipe
        close(fd[0]);              // close read end of pipe
        if ((execlp("grep", "grep", argv[2], NULL)) < 0) {
            perror("execlp");
            exit(EXIT_FAILURE);
        }
        exit(EXIT_SUCCESS);
    }
    close(fd[0]);
    close(fd[1]);

    while (wait(NULL) > 0); // wait for all children

    return EXIT_SUCCESS;
}
