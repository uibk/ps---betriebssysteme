#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/select.h>
#include <unistd.h>
#include <errno.h>

#define SERVERS 3
#define PIPE_BUF 4096

int main(int argc, char const *argv[]) {
    char *fifos[SERVERS] = {"/tmp/webserver_fifo", "/tmp/database_fifo", "/tmp/middleware_fifo"};
    int fifo_fds[SERVERS];

    char output[80];

    for (int i = 0; i < SERVERS; i++) {
        if ((mkfifo(fifos[i], 0666) == -1)) {
            if (errno == EEXIST) {
                // do nothing
            } else {
                perror("mkfifo()");
                exit(EXIT_FAILURE);
            }
        }
    }

    fd_set fds;
    int maxfd = 0;

    while (1) {
        FD_ZERO(&fds);
        for (int i = 0; i < SERVERS; i++) {
            if ((fifo_fds[i] = open(fifos[i], O_NONBLOCK)) == -1) {
                perror("open()");
                exit(EXIT_FAILURE);
            }
            FD_SET(fifo_fds[i], &fds);
            maxfd = fifo_fds[i] > maxfd ? fifo_fds[i] : maxfd;
        }
        select(maxfd + 1, &fds, NULL, NULL, NULL);

        for (int i = 0; i < SERVERS; i++) {
            if (FD_ISSET(fifo_fds[i], &fds)) {
                if (read(fifo_fds[i], output, sizeof(output)) > 0) {
                    printf("%s\n", output);
                }
            }
        }
    }
    return EXIT_SUCCESS;
}
