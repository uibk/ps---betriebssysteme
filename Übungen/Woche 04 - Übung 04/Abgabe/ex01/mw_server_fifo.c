#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <time.h>

#define PIPE_BUF 4096

int main(int argc, char const *argv[]) {
    char *fifo_name = "/tmp/middleware_fifo";
    srand(time(NULL));
    char input[PIPE_BUF];
    int i = 1;
    while (1) {
        int fd;
        int r = rand() % 7;
        // Opens fifo in WRITE ONLY mode
        if ((fd = open(fifo_name, O_WRONLY)) == -1) {
            perror("open()");
            exit(EXIT_FAILURE);
        }

        sleep(r);
        sprintf(input, "[middleware] message %d from middleware-server", i);
        if (strlen(input) < PIPE_BUF) {
            write(fd, input, strlen(input) + 1);
        }
        close(fd);
        i++;
    }
    return EXIT_SUCCESS;
}

